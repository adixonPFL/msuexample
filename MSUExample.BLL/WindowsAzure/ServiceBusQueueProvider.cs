﻿#region File Header
// <copyright file="ServiceBusQueueProvider.cs" company="PFL">
//   PFL 04/2017
// </copyright>
// <summary>
//   A way to communicate with Azure's ServiceBusQueue
// </summary>
#endregion

namespace MSUExample.BLL.WindowsAzure
{
    using Microsoft.ServiceBus.Messaging;
    using MSUExample.BO.Interfaces;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class ServiceBusQueueProvider : IQueueProvider
    {
        private const string _connectionString = "Endpoint=sb://msuexample.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=RbeUJQlt+YWQr0vxBoOhVOjD/VM6u9LxUwQ1bQAxShw=;EntityPath=exampleQueue";
        private Lazy<QueueClient> _client;

        private QueueClient Client => _client.Value;

        public ServiceBusQueueProvider()
        {
            _client = new Lazy<QueueClient>(() => QueueClient.CreateFromConnectionString(_connectionString));
        }

        public async Task<string> Enqueue(string message, IDictionary<string, string> properties = null)
        {
            BrokeredMessage brokeredMessage = new BrokeredMessage(message);

            foreach(KeyValuePair<string, string> property in properties ?? new Dictionary<string, string>())
            {
                brokeredMessage.Properties[property.Key] = property.Value;
            }
            
            await Client.SendAsync(brokeredMessage).ConfigureAwait(false);

            return brokeredMessage.MessageId;
        }
    }
}
