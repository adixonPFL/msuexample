﻿#region File Header
// <copyright file="BlobStorageProvider.cs" company="PFL">
//   PFL 04/2017
// </copyright>
// <summary>
//   A way to communicate with Azure's Blob Storage
// </summary>
#endregion

namespace MSUExample.BLL.WindowsAzure
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.WindowsAzure.Storage;
    using Microsoft.WindowsAzure.Storage.Blob;
    using MSUExample.BLL.Extensions;
    using MSUExample.BO;
    using MSUExample.BO.Interfaces;

    public class BlobStorageProvider : IStorageProvider
    {
        private const string _connectionString = "DefaultEndpointsProtocol=https;AccountName=msuexample;AccountKey=icYaOkL7FRaH9FyCE1P003lcDP9HJp8xYg4tK/w4oOHx3cpQeDX5ULmorvHCFYHFknehndPp0UxjW7c3JsnmeQ==;EndpointSuffix=core.windows.net";
        private const string _containerName = "example-container";
        private readonly Lazy<CloudBlobClient> _client;

        private CloudBlobClient Client => _client.Value;

        public BlobStorageProvider()
        {
            // Create connection to storage account
            Lazy<CloudStorageAccount> storageAccount = new Lazy<CloudStorageAccount>(() => CloudStorageAccount.Parse(_connectionString));

            // Create client to communicate (and create the container if it doesn't exist)
            _client = new Lazy<CloudBlobClient>(() =>
            {
                CloudBlobClient client = storageAccount.Value.CreateCloudBlobClient();
                CloudBlobContainer container = client.GetContainerReference(_containerName);
                container.CreateIfNotExists();
                return client;
            });
        }

        public async Task<StorageObject> Create(string id, string content, IDictionary<string, string> metadata = null)
        {
            CloudBlobContainer container = Client.GetContainerReference(_containerName);

            // Get a reference to the blob, even if it doesn't exist
            CloudBlockBlob blob = container.GetBlockBlobReference(id);

            // Create or overwrite blob
            await blob.UploadTextAsync(content).ConfigureAwait(false);

            SetMetadata(ref blob, metadata);
            
            return await blob.ToStorageObject().ConfigureAwait(false);
        }

        public async Task<StorageObject> Read(string id)
        {
            CloudBlobContainer container = Client.GetContainerReference(_containerName);

            // Get a reference to the blob, even if it doesn't exist
            CloudBlockBlob blob = container.GetBlockBlobReference(id);
            
            if (!await blob.ExistsAsync().ConfigureAwait(false))
            {
                throw new InvalidOperationException("Cannot read blob that does not exist.");
            }

            return await blob.ToStorageObject().ConfigureAwait(false);
        }

        public async Task<StorageObject> Update(string id, string content, IDictionary<string, string> metadata = null)
        {
            CloudBlobContainer container = Client.GetContainerReference(_containerName);

            // Get a reference to the blob, even if it doesn't exist
            CloudBlockBlob blob = container.GetBlockBlobReference(id);

            if (!await blob.ExistsAsync().ConfigureAwait(false))
            {
                throw new InvalidOperationException("Cannot update blob that does not exist.");
            }

            // Overwrite blob
            await blob.UploadTextAsync(content).ConfigureAwait(false);

            SetMetadata(ref blob, metadata, false);

            return await blob.ToStorageObject().ConfigureAwait(false);
        }

        public async Task Delete(string id)
        {
            CloudBlobContainer container = Client.GetContainerReference(_containerName);

            // Get a reference to the blob, even if it doesn't exist
            CloudBlockBlob blob = container.GetBlockBlobReference(id);

            // Delete the blob. We don't need to check if it exists because it's about to be deleted
            await blob.DeleteAsync().ConfigureAwait(false);
        }

        private void SetMetadata(ref CloudBlockBlob blob, IDictionary<string, string> metadata, bool clearMetadata = true)
        {
            if (clearMetadata)
            { 
                // Clear the metadata
                blob.Metadata.Clear();
            }

            // Iterate through metadata passed in
            foreach (KeyValuePair<string, string> meta in metadata ?? new Dictionary<string, string>())
            {
                blob.Metadata[meta.Key] = meta.Value;
            }

            // Update metadata in blob
            blob.SetMetadata();
        }
    }
}