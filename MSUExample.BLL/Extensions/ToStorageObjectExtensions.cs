﻿#region File Header
// <copyright file="ToStorageObject.cs" company="PFL">
//   PFL 04/2017
// </copyright>
// <summary>
//   Contains extension methods required to convert to provider agnostic StorageObject
// </summary>
#endregion

namespace MSUExample.BLL.Extensions
{
    using Microsoft.WindowsAzure.Storage.Blob;
    using MSUExample.BO;
    using System.Threading.Tasks;

    public static class ToStorageObjectExtensions
    {
        public static async Task<StorageObject> ToStorageObject(this CloudBlockBlob blob)
        {
            return new StorageObject
            {
                Id = blob.Name,
                Content = await blob.DownloadTextAsync().ConfigureAwait(false),
                Metadata = blob.Metadata
            };
        }
    }
}