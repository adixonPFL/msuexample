﻿#region File Header
// <copyright file="IStorageProvider.cs" company="PFL">
//   PFL 04/2017
// </copyright>
// <summary>
//   An interface used to do simple CRUD operations
//   to be implemented by concrete providers
// </summary>
#endregion

namespace MSUExample.BO.Interfaces
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IStorageProvider
    {
        Task<StorageObject> Create(string id, string content, IDictionary<string, string> metadata = null);
        Task<StorageObject> Read(string id);
        Task<StorageObject> Update(string id, string content, IDictionary<string, string> metadata = null);
        Task Delete(string id);
    }
}