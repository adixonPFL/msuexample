﻿#region File Header
// <copyright file="IQueueService.cs" company="PFL">
//   PFL 04/2017
// </copyright>
// <summary>
//   An interface used to enforce simple queue actions
// </summary>
#endregion

namespace MSUExample.BO.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IQueueProvider
    {
        Task<string> Enqueue(string message, IDictionary<string, string> properties = null);
    }
}