﻿#region File Header
// <copyright file="StorageObject.cs" company="PFL">
//   PFL 04/2017
// </copyright>
// <summary>
//   A provider-agnostic object used when communicating with
//   storage providers
// </summary>
#endregion

namespace MSUExample.BO
{
    using System.Collections.Generic;

    public class StorageObject
    {
        public string Id { get; set; }
        public string Content { get; set; }
        public IDictionary<string, string> Metadata { get; set; }
    }
}
