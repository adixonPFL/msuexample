﻿#region File Header
// <copyright file="Functions.cs" company="PFL">
//   PFL 04/2017
// </copyright>
// <summary>
//   Contains functions for the QueueReceiver Webjob
// </summary>
#endregion

namespace QueueReceiver
{
    using System;
    using Microsoft.Azure.WebJobs;
    using Microsoft.ServiceBus.Messaging;

    public class Functions
    {
        private const string _queueName = "exampleQueue";

        public static void Listen([ServiceBusTrigger(_queueName)] BrokeredMessage message)
        {
            Console.WriteLine("Message Received:");
            Console.WriteLine($"        Id: {message.MessageId}");
            Console.WriteLine($"   Content: {message.GetBody<string>()}");
        }
    }
}