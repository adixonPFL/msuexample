﻿#region File Header
// <copyright file="Program.cs" company="PFL">
//   PFL 04/2017
// </copyright>
// <summary>
//   Sets up the webjob
// </summary>
#endregion

namespace QueueReceiver
{
    using Microsoft.Azure.WebJobs;
    using Microsoft.Azure.WebJobs.ServiceBus;

    class Program
    {
        private const string _connectionString = "Endpoint=sb://msuexample.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=RbeUJQlt+YWQr0vxBoOhVOjD/VM6u9LxUwQ1bQAxShw=";

        static void Main(string[] args)
        {
            JobHostConfiguration config = new JobHostConfiguration();
            ServiceBusConfiguration serviceBusConfig = new ServiceBusConfiguration
            {
                ConnectionString = _connectionString
            };
            config.UseServiceBus(serviceBusConfig);

            JobHost host = new JobHost(config);
            host.RunAndBlock();
        }
    }
}
