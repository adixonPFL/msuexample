﻿#region File Header
// <copyright file="StorageController.cs" company="PFL">
//   PFL 04/2017
// </copyright>
// <summary>
//   Contains extension methods required to convert to provider agnostic StorageObject
// </summary>
#endregion

namespace MSUExample.API.Controllers
{
    using MSUExample.BLL.WindowsAzure;
    using MSUExample.BO;
    using MSUExample.BO.Interfaces;
    using System.Threading.Tasks;
    using System.Web.Http;

    public class StorageController : ApiController
    {
        [HttpPost]
        public async Task<IHttpActionResult> Create(string id, [FromBody] string content)
        {
            IStorageProvider provider = new BlobStorageProvider();

            StorageObject obj = await provider.Create(id, content).ConfigureAwait(false);

            return Ok($"Created object with Id: {obj.Id}, Content: {obj.Content}");
        }

        [HttpGet]
        public async Task<IHttpActionResult> Get(string id)
        {
            IStorageProvider provider = new BlobStorageProvider();

            StorageObject obj = await provider.Read(id).ConfigureAwait(false);

            return Ok($"Content: {obj.Content}");
        }

        [HttpPut]
        public async Task<IHttpActionResult> Get(string id, [FromBody] string content)
        {
            IStorageProvider provider = new BlobStorageProvider();

            StorageObject obj = await provider.Update(id, content).ConfigureAwait(false);

            return Ok($"Updated object with Id: {obj.Id}, Content: {obj.Content}");
        }

        [HttpDelete]
        public async Task<IHttpActionResult> Delete(string id)
        {
            IStorageProvider provider = new BlobStorageProvider();

            await provider.Delete(id).ConfigureAwait(false);

            return Ok($"If the object with id '{id}' was there, it's gone now.");
        }
    }
}
