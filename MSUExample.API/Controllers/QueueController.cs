﻿#region File Header
// <copyright file="QueueController.cs" company="PFL">
//   PFL 04/2017
// </copyright>
// <summary>
//   Contains extension methods required to convert to provider agnostic StorageObject
// </summary>
#endregion

namespace MSUExample.API.Controllers
{
    using MSUExample.BLL.WindowsAzure;
    using MSUExample.BO.Interfaces;
    using System.Threading.Tasks;
    using System.Web.Http;

    public class QueueController : ApiController
    {
        [HttpPost]
        public async Task<IHttpActionResult> Post([FromBody] string message)
        {
            IQueueProvider queueProvider = new ServiceBusQueueProvider();

            string id = await queueProvider.Enqueue(message).ConfigureAwait(false);

            return Ok($"Message Queued with Id: {id}"); 
        }
    }
}
